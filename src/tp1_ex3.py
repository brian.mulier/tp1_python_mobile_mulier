import cv2


def main():
    img = cv2.imread('../Data-TP/waves.jpg', 0)
    base_hist = calc_norm_hist(img)

    compared_images = ["../Data-TP/beach.jpg", "../Data-TP/dog.jpg", "../Data-TP/polar.jpg", "../Data-TP/bear.jpg",
                       "../Data-TP/lake.jpg", "../Data-TP/moose.jpg"]

    most_similar_img = None

    for img_url in compared_images:
        hist = calc_norm_hist(cv2.imread(img_url, 0))
        hist_comparison = cv2.compareHist(base_hist, hist, cv2.HISTCMP_CORREL)
        split = img_url.split('/')
        real_name = split[len(split) - 1]
        if most_similar_img is None or hist_comparison > most_similar_img.get('similarity'):
            most_similar_img = {"name": real_name, "similarity": hist_comparison}

        print(f"{real_name} : {hist_comparison}")

    print(f"\n\nThe most similar image is {most_similar_img.get('name')} with "
          f"{most_similar_img.get('similarity')*100}% similarity.")


def calc_norm_hist(img):
    base_hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    cv2.normalize(base_hist, base_hist, 0, 1, cv2.NORM_MINMAX)
    return base_hist


if __name__ == "__main__":
    main()
