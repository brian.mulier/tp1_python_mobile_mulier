import cv2
import imutils


def save_webcam(out_path, fps, mirror=False):
    previous_frame = None
    # Capturing video from webcam:
    cap = cv2.VideoCapture(0)
    current_frame = 0
    # Get current width of frame
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
    # Get current height of frame
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    out = cv2.VideoWriter(out_path, fourcc, fps, (int(width), int(height)))
    while cap.isOpened():
        text = "Nothing detected"
        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret:
            if mirror:
                # Mirror the output video frame
                frame = cv2.flip(frame, 1)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            if previous_frame is None:
                previous_frame = gray
                continue
            frame_delta = cv2.absdiff(previous_frame, gray)
            thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]
            thresh = cv2.dilate(thresh, None, iterations=2)
            contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
            contours = imutils.grab_contours(contours)
            # loop over the contours
            for c in contours:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < 100:
                    continue

                # compute the bounding box for the contour, draw it on the frame,
                # and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                text = "Something is moving"

            cv2.putText(frame, text, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
            print(text)
            # Saves for video
            out.write(frame)
            cv2.imshow('B&W detection', frame_delta)
            cv2.imshow('Color detection', frame)
            # Display the resulting frame
            previous_frame = gray
        else:
            break
        if cv2.waitKey(1) & 0xFF == ord('q'):  # if 'q' is pressed then quit
            break
        # To stop duplicate images
        current_frame += 1
    # When everything done, release the capture
    cap.release()
    out.release()
    cv2.destroyAllWindows()


def main():
    save_webcam('output.avi', 30.0, mirror=True)


if __name__ == '__main__':
    main()
