import cv2
import numpy as np

def show_colors(path):
    img = cv2.imread(path)
    (b, g, r) = cv2.split(img)
    channel = np.zeros(img.shape[:2], np.uint8)
    cv2.imshow("Red", np.dstack((channel, channel, r)))
    cv2.imshow("Green", np.dstack((channel, g, channel)))
    cv2.imshow("Blue", np.dstack((b, channel, channel)))

    if cv2.waitKey(0) & 0xFF == ord('q'):
        cv2.destroyAllWindows()


def main():
    show_colors("../Data-TP/bgr.png")


if __name__ == '__main__':
    main()
