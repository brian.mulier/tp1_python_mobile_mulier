from PIL import Image
import numpy as np
from IPython.display import display
from src.KNNClassifier import KNNClassifier


def main():
    basedir_data = "../data/"
    rel_path = basedir_data + "cifar-10-batches-py/"

    x = unpickle(rel_path + 'data_batch_1')
    img_data = x['data']
    img_label_orig = img_label = x['labels']
    img_label = np.array(img_label).reshape(-1, 1)

    print(img_data)
    print('shape', img_data.shape)

    print(img_label)
    print('shape', img_label.shape)

    test_x = unpickle(rel_path + 'test_batch')
    test_data = test_x['data']
    test_label = test_x['labels']
    test_label = np.array(test_label).reshape(-1, 1)

    sample_img_data = img_data[0:10, :]
    print(sample_img_data)
    print('shape', sample_img_data.shape)

    batch = unpickle(rel_path + 'batches.meta')
    meta = batch['label_names']
    print(meta)

    def pred_label_fn(i, original):
        return original + '::' + meta[YPred[i]]

    data_point_no = 10
    sample_test_data = test_data[:data_point_no, :]
    # To keep librarie's classifier
    # nbrs = KNeighborsClassifier(n_neighbors=3, algorithm='brute').fit(img_data, img_label_orig)

    # My own classifier
    nbrs = KNNClassifier(n_neighbors=5, algorithm='brute').fit(img_data, img_label_orig)
    YPred = nbrs.predict(sample_test_data)

    for i in range(0, len(YPred)):
        show_img(sample_test_data, test_label, meta, i, label_fn=pred_label_fn)

def default_label_fn(i, original):
    return original


def show_img(img_arr, label_arr, meta, index, label_fn=default_label_fn):
    one_img = img_arr[index, :]
    r = one_img[:1024].reshape(32, 32)
    g = one_img[1024:2048].reshape(32, 32)
    b = one_img[2048:].reshape(32, 32)
    rgb = np.dstack([r, g, b])
    img = Image.fromarray(np.array(rgb), 'RGB')
    display(img)
    print(label_fn(index, meta[label_arr[index][0]]))


def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='latin1')
    return dict

if __name__ == "__main__":
    main()
