import cv2


def calc_norm_hist(img):
    base_hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    cv2.normalize(base_hist, base_hist, 0, 1, cv2.NORM_MINMAX)
    return base_hist


class KNNClassifier(object):
    def __init__(self, n_neighbors=3, algorithm="brute"):
        self.training_data = None
        self.n_neighbors = n_neighbors
        self.algorithm = algorithm

    def fit(self, training_images, training_values):
        self.training_data = [(training_image, training_value, calc_norm_hist(training_image))
                              for training_image, training_value in zip(training_images, training_values)]
        return self

    def predict(self, images_to_predict):
        if self.training_data is not None:
            if self.algorithm == 'brute':
                res = []
                for img_to_predict in images_to_predict:
                    hist_comparisons = []
                    for(img, value, training_hist) in self.training_data:
                        hist_comparisons.append((
                            cv2.compareHist(calc_norm_hist(img_to_predict), training_hist, cv2.HISTCMP_CORREL), value))
                    hist_comparisons = sorted(hist_comparisons, key=lambda tup:tup[0], reverse=True)
                    closest_labels = dict()
                    for idx in range(self.n_neighbors):
                        label = hist_comparisons[idx][1]
                        if label in closest_labels:
                            closest_labels[label] += 1
                        else:
                            closest_labels[label] = 1

                    closest_labels = sorted(closest_labels.items(), key=lambda tup: tup[1], reverse=True)
                    res.append(closest_labels[0][0])
                return res
