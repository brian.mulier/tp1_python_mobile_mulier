import cv2


def main():
    img_to_find = cv2.imread('../Data-TP/Template matching/baboon_head.jpg')
    hist_img_to_find = calc_norm_hist(img_to_find)
    area_height, area_width = img_to_find.shape[:2]

    img = cv2.imread('../Data-TP/Template matching/baboon.jpg')
    img_height, img_width = img.shape[:2]

    offset_x, offset_y = 0, 0

    found_area = None
    displayed = False
    step = 2
    while True:
        if cv2.waitKey(1) & 0xFF == ord('q'):  # if 'q' is pressed then quit
            break
        while offset_y + area_height - step != img_height:
            offset_x = 0
            if offset_y + area_height > img_height:
                offset_y = img_height - area_height
            bottom_y = offset_y + area_height

            while offset_x + area_width - step != img_width:
                if offset_x + area_width > img_width:
                    offset_x = img_width - area_width
                right_x = offset_x + area_width

                tested_area = img[offset_y:bottom_y, offset_x:right_x]
                tested_hist = calc_norm_hist(tested_area)
                hist_similarity = cv2.compareHist(hist_img_to_find, tested_hist, cv2.HISTCMP_CORREL)
                if hist_similarity > 0.7 and (found_area is None or found_area.get("similarity") < hist_similarity):
                    found_area = {"x": offset_x, "y": offset_y, "similarity": hist_similarity}
                offset_x += step
            offset_y += step

        if found_area is not None:
            if not displayed:
                cv2.rectangle(img, (found_area.get("x"), found_area.get("y")),
                              (found_area.get("x") + area_width, found_area.get("y") + area_height), (0, 255, 0), 2)
                cv2.imshow("Template detector with histograms", img)
                print(f"Similarity : {found_area.get('similarity')}")
                displayed = True
        else:
            print("No matches have been found")
    cv2.destroyAllWindows()


def calc_norm_hist(img):
    base_hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    cv2.normalize(base_hist, base_hist, 0, 1, cv2.NORM_MINMAX)
    return base_hist


if __name__ == "__main__":
    main()
